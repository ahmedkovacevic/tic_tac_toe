cmake_minimum_required(VERSION 2.8.1)

#message(STATUS "(. ${.})")

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
endif(NOT CMAKE_BUILD_TYPE)

project(sdk)

add_definitions(
  -D_CRT_SECURE_NO_WARNINGS)
  
set(CMAKE_CXX_STANDARD 17)

include_directories(.)

include_directories(
  ../src_sdk)

file (GLOB APP_SOURCES ../sdk_src/*.cpp)
file (GLOB APP_HEADERS ../sdk_h/*.h)

  
add_library(
  sdk ${APP_HEADERS} ${APP_SOURCES})

source_group("Header Files" FILES ${APP_HEADERS})  
source_group("Source Files" FILES ${APP_SOURCES})

if( WIN32 )
  target_link_libraries(
    sdk
    odbc32
    odbccp32
    winmm
    ws2_32   
  )
endif( WIN32 )

if ( UNIX )
  target_link_libraries(
    sdk
    pthread
    dl
    curl
  )
endif( UNIX )

