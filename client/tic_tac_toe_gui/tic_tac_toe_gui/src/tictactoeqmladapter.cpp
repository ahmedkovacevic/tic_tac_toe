#include "tictactoeqmladapter.h"
#include "rapidjson/document.h"

#include <iostream>

GameInfo::GameInfo()
    : m_gameId(-1)
    , m_player1Name("")
    , m_player2Name("")
    , m_gameStatus("")
{}

GameInfo::GameInfo(int gameId, std::string player1Name, std::string player2Name, GameState state)
    : m_gameId(gameId)
    , m_player1Name(QString::fromStdString(player1Name))
    , m_player2Name(QString::fromStdString(player2Name))
{
    switch (state)
    {
    case GAME_DRAWN:
        m_gameStatus = "Game drawn";
        break;
    case GAME_PLAYER1_WON:
        m_gameStatus = m_player1Name + " won";
        break;
    case GAME_PLAYER2_WON:
        m_gameStatus = m_player2Name + " won";
        break;
    default:
        m_gameStatus = "";
        break;
    }
}

PlayerInfo::PlayerInfo()
    : m_userName("NewPlayer")
    , m_gamesWon(0)
    , m_gamesDrawn(0)
    , m_gamesLost(0)
    , m_score(0)
{}

PlayerInfo::PlayerInfo(std::string name, int gamesWon, int gamesDrawn, int gamesLost)
    : m_userName(name)
    , m_gamesWon(gamesWon)
    , m_gamesDrawn(gamesDrawn)
    , m_gamesLost(gamesLost)
    , m_score(3*gamesWon+gamesDrawn)
{}

TicTacToeQMLAdapter::TicTacToeQMLAdapter(QObject *parent)
    : QObject(parent)
{
    m_timer = new QTimer();
    connect(m_timer, &QTimer::timeout, this, &TicTacToeQMLAdapter::tryToConnect);
}

void TicTacToeQMLAdapter::login()
{
    sdk& sdkInstance = sdk::getInstance();
    sdkInstance.setIpAddr(); // localhost as default

    m_timer->start(1000);
}

void TicTacToeQMLAdapter::logout()
{
    sdk::getInstance().logout();
}

void TicTacToeQMLAdapter::tryToConnect()
{
    sdk& sdkInstance = sdk::getInstance();

    if(sdkInstance.login())
    {
        m_timer->stop();
        emit connectedToServer();
    }
}

void TicTacToeQMLAdapter::addPlayerInfoToListAndSortByScore(PlayerInfo *playerInfo)
{
    for(QList<PlayerInfo*>::const_iterator iter=m_playerInfoList.cbegin(); iter!= m_playerInfoList.cend(); ++iter)
    {
        if((*iter)->getScore() < playerInfo->getScore())
        {
            m_playerInfoList.insert(iter, playerInfo);
            return;
        }
    }
    m_playerInfoList.push_back(playerInfo);
}

void TicTacToeQMLAdapter::getAllPlayers()
{
    m_playerInfoList.clear();
    sdk& sdkInstance = sdk::getInstance();
    std::vector<std::string> allPlayers = sdkInstance.getListOfAllPlayers();
    for(size_t i = 0; i < allPlayers.size(); ++i)
    {
        std::string player = allPlayers[i];
        std::cout << player << std::endl;

        size_t startOfUserName = player.find("=");
        size_t stopOfUserName = player.find("&");
        std::string userName = player.substr(startOfUserName + 1, stopOfUserName - startOfUserName - 1);

        size_t startOfGamesWon = player.find("=", stopOfUserName);
        size_t stopOfGamesWon = player.find("&", startOfGamesWon);
        int gamesWon = std::atoi(player.substr(startOfGamesWon + 1, stopOfGamesWon - startOfGamesWon - 1).c_str());

        size_t startOfGamesDrawn = player.find("=", stopOfGamesWon);
        size_t stopOfGamesDrawn = player.find("&", startOfGamesDrawn);
        int gamesDrawn = std::atoi(player.substr(startOfGamesDrawn + 1, stopOfGamesDrawn - startOfGamesDrawn - 1).c_str());

        size_t startOfGamesLost = player.find("=", stopOfGamesDrawn);
        size_t stopOfGamesLost = player.find("&", startOfGamesLost);
        int gamesLost = std::atoi(player.substr(startOfGamesLost + 1, stopOfGamesLost - startOfGamesLost - 1).c_str());

        PlayerInfo* playerInfo = new PlayerInfo(userName, gamesWon, gamesDrawn, gamesLost);
        addPlayerInfoToListAndSortByScore(playerInfo);
    }

    emit playerListChanged();
}

void TicTacToeQMLAdapter::getAllGames()
{
    m_gameInfoList.clear();
    sdk& sdkInstance = sdk::getInstance();
    std::vector<std::string> allGames = sdkInstance.getListOfAllGames();

    for(size_t i = 0; i < allGames.size(); ++i)
    {
        std::string game = allGames[i];

        size_t startOfGameId = game.find("=");
        size_t stopOfGameId = game.find("&");
        int gameId = std::atoi(game.substr(startOfGameId + 1, stopOfGameId - startOfGameId - 1).c_str());

        size_t startOfPlayer1Name = game.find("=", stopOfGameId);
        size_t stopOfPlayer1Name = game.find("&", startOfPlayer1Name);
        std::string player1Name = game.substr(startOfPlayer1Name + 1, stopOfPlayer1Name - startOfPlayer1Name - 1);

        size_t startOfPlayer2Name = game.find("=", stopOfPlayer1Name);
        size_t stopOfPlayer2Name = game.find("&", startOfPlayer2Name);
        std::string player2Name = game.substr(startOfPlayer2Name + 1, stopOfPlayer2Name - startOfPlayer2Name - 1);

        size_t startOfGameStatus = game.find("=", stopOfPlayer2Name);
        size_t stopOfGameStatus = game.find("&", startOfGameStatus);
        int gameStatus = std::atoi(game.substr(startOfGameStatus + 1, stopOfGameStatus - startOfGameStatus - 1).c_str());

        GameInfo* gameInfo = new GameInfo(gameId, player1Name, player2Name, GameState(gameStatus));
        m_gameInfoList.push_back(gameInfo);
    }

    emit gameListChanged();
}

void TicTacToeQMLAdapter::makeMove(int index)
{
    sdk& sdkInstance = sdk::getInstance();

    m_gameBoard = sdkInstance.makeMove(index);

    if(m_gameBoard.gameStatus != sdk::INVALID_MOVE)
    {
        std::string gameBoard = m_gameBoard.board;

        size_t startOfIndex = 0;
        size_t stopOfIndex = 0;

        m_gameBoardList.clear();
        for (int i = 0; i < 9; ++i)
        {
            startOfIndex = gameBoard.find("=", stopOfIndex);
            stopOfIndex = gameBoard.find("&", startOfIndex);

            if(gameBoard.substr(startOfIndex + 1, stopOfIndex - startOfIndex - 1) == "X")
            {
                QString sign = QString("tic.png");
                m_gameBoardList.push_back(sign);
            }
            else if(gameBoard.substr(startOfIndex + 1, stopOfIndex - startOfIndex - 1) == "O")
            {
                QString sign = QString("tac.png");
                m_gameBoardList.push_back(sign);
            }
            else
            {
                QString sign = QString("white.png");
                m_gameBoardList.push_back(sign);
            }
        }

        if(m_gameBoard.gameStatus > 1)
        {
            // if game is finished update lists of games and players
            getAllPlayers();
            getAllGames();
        }
        emit gameBoardChanged();
    }
    emit gameStatusChanged();
}

QString TicTacToeQMLAdapter::getCellPng(int index)
{
    if(index < m_gameBoardList.size())
    {
        return m_gameBoardList[index];
    }
    return "white.png"; // default png
}

int TicTacToeQMLAdapter::getGameStatus()
{
    return m_gameBoard.gameStatus;
}

void TicTacToeQMLAdapter::registerGame(QString player1, QString player2)
{
    std::cout << "Player name 1: " << player1.toStdString() << " Player name 2: " << player2.toStdString() << std::endl;
    m_player1 = player1;
    m_player2 = player2;
    emit gameRegistered(sdk::getInstance().registerGame(m_player1.toStdString(), m_player2.toStdString()));
}

void TicTacToeQMLAdapter::playAgain()
{
    emit gameRegistered(sdk::getInstance().registerGame(m_player1.toStdString(), m_player2.toStdString()));
}

QQmlListProperty<PlayerInfo> TicTacToeQMLAdapter::getPlayerList()
{
    return QQmlListProperty<PlayerInfo>(this, &m_playerInfoList);
}

QQmlListProperty<GameInfo> TicTacToeQMLAdapter::getGameList()
{
    return QQmlListProperty<GameInfo>(this, &m_gameInfoList);
}
