#ifndef TICTACTOEQMLADAPTER_H
#define TICTACTOEQMLADAPTER_H

#include <QObject>
#include <QQmlListProperty>
#include <QTimer>

#include "sdk.h"

typedef enum
{
    GAME_DRAWN = 0,
    GAME_PLAYER1_WON = 1,
    GAME_PLAYER2_WON = 2
} GameState;

class GameInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int gameId READ getGameId CONSTANT)
    Q_PROPERTY(QString player1Name READ getPlayer1Name CONSTANT)
    Q_PROPERTY(QString player2Name READ getPlayer2Name CONSTANT)
    Q_PROPERTY(QString gameStatus READ getGameStatus CONSTANT)

public:
    GameInfo();
    GameInfo(int gameId, std::string player1Name, std::string player2Name, GameState state);

    int getGameId() { return m_gameId; }
    QString getPlayer1Name() { return m_player1Name; }
    QString getPlayer2Name() { return m_player2Name; }
    QString getGameStatus() { return m_gameStatus; }

private:
    int m_gameId;
    QString m_player1Name;
    QString m_player2Name;
    QString m_gameStatus;
};

class PlayerInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString playerName READ getPlayerName NOTIFY playerNameChanged)
    Q_PROPERTY(int gamesWon READ getGamesWon NOTIFY gamesWonChanged)
    Q_PROPERTY(int gamesDrawn READ getGamesDrawn NOTIFY gamesDrawnChanged)
    Q_PROPERTY(int gamesLost READ getGamesLost NOTIFY gamesLostChanged)
public:
    PlayerInfo();
    PlayerInfo(std::string name, int gamesWon, int gamesDrawn, int gamesLost);

    QString getPlayerName() { return QString::fromStdString(m_userName); }
    int getGamesWon() { return m_gamesWon; }
    int getGamesDrawn() { return m_gamesDrawn; }
    int getGamesLost() { return m_gamesLost; }
    int getScore() { return m_score; }

signals:
    void playerNameChanged();
    void gamesWonChanged();
    void gamesDrawnChanged();
    void gamesLostChanged();

private:
    std::string m_userName;
    int m_gamesWon;
    int m_gamesDrawn;
    int m_gamesLost;
    int m_score;
};

class TicTacToeQMLAdapter : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<PlayerInfo> playerList READ getPlayerList NOTIFY playerListChanged)
    Q_PROPERTY(QQmlListProperty<GameInfo> gameList READ getGameList NOTIFY gameListChanged)
public:
    TicTacToeQMLAdapter(QObject *parent = nullptr);
    ~TicTacToeQMLAdapter() { sdk::getInstance().logout(); }

    Q_INVOKABLE void login();
    Q_INVOKABLE void getAllPlayers();
    Q_INVOKABLE void getAllGames();
    Q_INVOKABLE void makeMove(int index);
    Q_INVOKABLE int getGameStatus();
    Q_INVOKABLE void registerGame(QString player1, QString player2);
    Q_INVOKABLE void playAgain();
    Q_INVOKABLE QString getCellPng(int index);
    Q_INVOKABLE void logout();

    QQmlListProperty<PlayerInfo> getPlayerList();
    QQmlListProperty<GameInfo> getGameList();


signals:

    void connectedToServer();
    void gameRegistered(bool gameRegistered);

    void gameBoardChanged();
    void gameStatusChanged();

    void playerListChanged();
    void gameListChanged();

private slots:
    void tryToConnect();

private:

    void addPlayerInfoToListAndSortByScore(PlayerInfo* playerInfo);

private:
    QTimer* m_timer;
    sdk::GameBoard m_gameBoard;

    QString m_player1;
    QString m_player2;

    QVector<QString> m_gameBoardList;
    QList<PlayerInfo*> m_playerInfoList;
    QList<GameInfo*> m_gameInfoList;
};

#endif // TICTACTOEQMLADAPTER_H
