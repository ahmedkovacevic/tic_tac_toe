#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "tictactoeqmladapter.h"


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    TicTacToeQMLAdapter ticTacToeQmlAdapter;
    qmlRegisterType<PlayerInfo>("com.mohabuje.package", 0, 1, "PlayerInfo");
    qmlRegisterType<GameInfo>("com.mohabuje.package", 0, 1, "GameInfo");
    engine.rootContext()->setContextProperty("ticTacToeAdapter", &ticTacToeQmlAdapter);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
