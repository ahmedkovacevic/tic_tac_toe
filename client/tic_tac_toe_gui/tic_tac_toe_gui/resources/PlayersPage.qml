import QtQuick 2.15
import QtQuick.Controls 2.15

Page
{
    title: qsTr("Scoreboard")

    Component.onCompleted: ticTacToeAdapter.getAllPlayers()

    ListView
    {
        id: listView
        anchors.fill: parent
        spacing: 5

        readonly property int itemHeight: listView.height * 0.15

        model: ticTacToeAdapter.playerList

        delegate: Rectangle {

            height: listView.itemHeight
            width: listView.width
            radius: 10
            color: "silver"

            Text { id: playerName; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/2; anchors.left: parent.left; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.playerName }
            Text { id: gamesWon;   horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/6; anchors.left: playerName.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.gamesWon }
            Text { id: gamesDrawn; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/6; anchors.left: gamesWon.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.gamesDrawn }
            Text { id: gamesLost;  horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/6; anchors.left: gamesDrawn.right; anchors.right: parent.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.gamesLost }
        }

        header: Rectangle {
            height: listView.itemHeight
            width: listView.width
            radius: 10
            color: "forestgreen"
            z: 10

            Text { id: headerGameId;        horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/2; anchors.left: parent.left; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("Player name") }
            Text { id: headerPlayer1Name;   horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/6; anchors.left: headerGameId.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("W") }
            Text { id: headerPlayer2Name;   horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/6; anchors.left: headerPlayer1Name.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("D") }
            Text { id: headerGameState;     horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.6; width: parent.width/6; anchors.left: headerPlayer2Name.right; anchors.right: parent.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("L") }
        }
        headerPositioning: ListView.OverlayHeader
    }
}
