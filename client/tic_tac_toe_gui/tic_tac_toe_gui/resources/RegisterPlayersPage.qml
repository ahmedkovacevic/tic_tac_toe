import QtQuick 2.15
import QtQuick.Controls 2.15

Page {

    Connections{
        target: ticTacToeAdapter
        function onGameRegistered(gameRegistered)
        {
            if(gameRegistered === true)
            {
                stackView.push("GamePage.qml")
                registerPlayersForGame.text = "Play"
            }
        }
    }

    Column{
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height/2
        width: parent.width/2
        spacing: 10
        Rectangle {
            id: playerName1Placeholder
            height: parent.height/2
            width: parent.width
            radius: 10
            color: "silver"
            Text {
                width: parent.width
                height: parent.height/2
                font.pixelSize: 0.5*parent.height/2
                text: qsTr("Enter name for player 1:")
            }
            TextInput{
                id: playerName1
                anchors.bottom: parent.bottom
                width: parent.width
                height: parent.height/2
                font.pixelSize: 0.5*parent.height/2
                onTextChanged: {
                    if(playerName1.text.length >= 3)
                    {
                        playerName1Placeholder.border.color = "transparent"
                    }
                }
            }
        }

        Rectangle {
            id: playerName2Placeholder
            height: parent.height/2
            width: parent.width
            radius: 10
            color: "silver"
            Text {
                width: parent.width
                height: parent.height/2
                font.pixelSize: 0.5*parent.height/2
                text: qsTr("Enter name for player 2:")
            }
            TextInput{
                id: playerName2
                anchors.bottom: parent.bottom
                width: parent.width
                height: parent.height/2
                font.pixelSize: 0.5*parent.height/2
                onTextChanged: {
                    if(playerName2.text.length >= 3)
                    {
                        playerName2Placeholder.border.color = "transparent"
                    }
                }
            }
        }

        GenericButton{
            id: registerPlayersForGame
            width: parent.width
            height: parent.height/4
            text: "Play"

            onClicked: {
                if(playerName1.text.length >= 3)
                {
                    if(playerName2.text.length >= 3)
                    {
                        registerPlayersForGame.text = "Waiting for server response"
                        ticTacToeAdapter.registerGame(playerName1.text, playerName2.text)
                        playerName1.text = ""
                        playerName2.text = ""
                    }
                    else
                    {
                        playerName2Placeholder.border.color = "red"
                    }
                }
                else
                {
                    playerName1Placeholder.border.color = "red"
                }
            }
        }
    }
}
