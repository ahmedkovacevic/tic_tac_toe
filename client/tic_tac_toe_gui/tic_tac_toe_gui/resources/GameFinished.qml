import QtQuick 2.15
import QtQuick.Controls 2.15

Page {

    Component.onCompleted:
    {
        switch (ticTacToeAdapter.getGameStatus())
        {
        case 2:
            stackViewNotificationManager.show(qsTr("Game finished in a draw"), 3000)
            break
        case 3:
            stackViewNotificationManager.show(qsTr("Player1 won"), 3000)
            break
        case 4:
            stackViewNotificationManager.show(qsTr("Player2 won"), 3000)
            break
        }
    }

    Connections{
        target: ticTacToeAdapter
        function onGameRegistered(gameRegistered)
        {
            console.log("Game is registered!")
            if(gameRegistered === true)
            {
                if (stackView.depth > 1)
                {
                    stackView.pop()
                    stackView.push("GamePage.qml")
                }
            }
        }
    }

    Column
    {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height/2
        width: parent.width/2
        spacing: 10

//        Text {
//            id: outputText
//            text: qsTr("")
//            width: parent.width
//            height: parent.height/4
//            font.pixelSize: 0.5*parent.height/2
//        }

        GenericButton
        {
            id: playAgainButton

            width: parent.width
            height: parent.height*3/8
            text: "Play again"

            onClicked:
            {
                ticTacToeAdapter.playAgain()
            }
        }

        GenericButton
        {
            id: newGameButton

            width: parent.width
            height: parent.height*3/8
            text: "New game"

            onClicked:
            {
                if (stackView.depth > 1)
                {
                    stackView.pop()
                }
            }
        }
    }
}
