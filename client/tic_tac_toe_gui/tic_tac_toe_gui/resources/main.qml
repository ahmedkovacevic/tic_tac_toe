import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Stack")

    Connections{
        target: ticTacToeAdapter
        function onConnectedToServer()
        {
            if (stackView.depth > 1) {
                stackView.pop()
            }
        }
    }

    Component.onCompleted:
    {
        stackView.push("ConnectingPage.qml")
        ticTacToeAdapter.login();
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Previous games")
                width: parent.width
                onClicked: {
                    stackView.push("GamesPage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Scoreboard")
                width: parent.width
                onClicked: {
                    stackView.push("PlayersPage.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "RegisterPlayersPage.qml"
        anchors.fill: parent
    }

    StackView
        {
            id: stackViewNotificationManager
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.margins: 20
            height: parent.height * 0.3
            width: parent.width * 0.9
            initialItem: Item {}
            z: 999
            //we disable the StackView when nothing is in the stack because,
            //at Qt 5.9.3 the StackView from the QtQuick.Controls 2. steels the touch events
            enabled: stackViewNotificationManager.depth > 1

            //applied to the item that enters the stack when the item is pushed
            pushEnter: Transition
            {
                PropertyAnimation
                {
                    property: "opacity"
                    from: 0
                    to: 1
                    duration: 600
                }
            }

            //applied to the item that exits the stack when another item is pushed
            pushExit: Transition
            {
                PropertyAnimation
                {
                    property: "opacity"
                    from: 1
                    to: 0
                    duration: 600
                }
            }

            //applied to the item that enters the stack when another item is popped
            popEnter: Transition
            {
                PropertyAnimation
                {
                    property: "opacity"
                    from: 0
                    to: 1
                    duration: 600
                }
            }

            //applied to the item that exits the stack when the item is popped
            popExit: Transition
            {
                PropertyAnimation
                {
                    property: "opacity"
                    from: 1
                    to: 0
                    duration: 600
                }
            }

            Timer
            {
                id: closeNotificationTimer
                onTriggered: stackViewNotificationManager.popCurrentNotification()
            }

            function show(messageString, duration)
            {
                //stop the timer and set the new interval
                closeNotificationTimer.stop()
                closeNotificationTimer.interval = duration
                //push a new notification in the stack and save the duration in the property "interval"
                stackViewNotificationManager.push("GenericNotification.qml", {text: messageString, interval: duration})
                //restart the timer
                closeNotificationTimer.start()
            }

            function popCurrentNotification()
            {
                //removes the notification from the stack
                stackViewNotificationManager.pop()

                //check if last notification can be displayed
                if(stackViewNotificationManager.currentItem !== undefined &&
                   stackViewNotificationManager.currentItem.interval !== undefined)
                {
                    //stop the timer, set the saved interval from the last notification and restart the timer
                    closeNotificationTimer.stop()
                    closeNotificationTimer.interval = stackViewNotificationManager.currentItem.interval
                    closeNotificationTimer.start()
                }
            }
        }
}
