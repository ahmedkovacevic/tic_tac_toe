import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    title: qsTr("Game")

    Component.onCompleted:
    {
        for(let index = 0; index < 9; ++index)
        {
            listModel.append({portrait: "white.png"})
        }
    }

    Connections{
        target: ticTacToeAdapter
        function onGameBoardChanged()
        {
            listModel.clear()
            for(let index = 0; index < 9; ++index)
            {
                listModel.append({portrait: ticTacToeAdapter.getCellPng(index)})
            }
        }

        function onGameStatusChanged()
        {
            console.log("Game status: ", ticTacToeAdapter.getGameStatus())
            if(ticTacToeAdapter.getGameStatus() > 1)
            {
                stackView.pop()
                stackView.push("GameFinished.qml")
                listModel.clear()
                for(let index = 0; index < 9; ++index)
                {
                    listModel.append({portrait: "white.png"})
                }
            }
            else if(ticTacToeAdapter.getGameStatus() === 0)
            {
                stackViewNotificationManager.show(qsTr("Invalid move!"), 3000)
            }
        }
    }

    Image {
        id: boardImage
        anchors.fill: parent
        source: "background.png"
        z: 5
    }

    ListModel {
        id: listModel
    }

    GridView{
        id: gridView

        anchors.fill: parent
        cellWidth: parent.width/3
        cellHeight: parent.height/3

        readonly property int itemHeight: parent.height/3
        readonly property int itemWidth: parent.width/3

        model: listModel
        delegate: Rectangle {
            id: cell
            height: gridView.itemHeight
            width: gridView.itemWidth

            readonly property int indexOfCurrentCell: index
            Image {
                source: portrait
                height: gridView.itemHeight*0.8
                width: gridView.itemWidth*0.8
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }

            MouseArea{
                id: cellMouseArea
                anchors.fill: parent
                onClicked:{
                    console.log("Cell clicked", cell.indexOfCurrentCell+1);
                    ticTacToeAdapter.makeMove(cell.indexOfCurrentCell+1)
                }
            }
        }
    }
}
