import QtQuick 2.15
import QtQuick.Controls 2.15

Page {

    TextMetrics{
        id: textMetrics
        font.pointSize: 20
        text: qsTr("Connecting to server, please wait!")
    }

    Rectangle{
        width: textMetrics.width
        height: parent.height/3
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: "silver"
        radius: 10

        Text {
            text: textMetrics.text
            font: textMetrics.font
            anchors.bottom: busyIndicator.top
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        BusyIndicator{
            id: busyIndicator
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            running: true
        }
    }
}
