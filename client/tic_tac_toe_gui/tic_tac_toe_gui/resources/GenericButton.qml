import QtQuick 2.15

Rectangle{
    id: button
    radius: 10
    color: "transparent"

    property alias text: buttonText.text

    signal clicked()

    Rectangle{
        width: parent.width*0.98
        height: parent.height*0.94
        anchors.centerIn: parent
        radius: 10
        color: "royalblue"

        Text {
            id: buttonText
            anchors.fill: parent
            font.pixelSize: 0.5*parent.height
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea{
            anchors.fill: parent
            onPressed: {
                button.color = "lightsteelblue"
            }
            onReleased: {
                button.color = "transparent"
            }
            onClicked: button.clicked()
        }
    }
}
