import QtQuick 2.15
import QtQuick.Controls 2.15

Page
{
    title: qsTr("Games")

    Component.onCompleted: ticTacToeAdapter.getAllGames()

    ListView
    {
        id: listView
        anchors.fill: parent
        spacing: 5

        readonly property int itemHeight: listView.height * 0.15

        model: ticTacToeAdapter.gameList

        delegate: Rectangle {

            height: listView.itemHeight
            width: listView.width
            radius: 10
            color: "silver"

            Text { id: gameId;      horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width/9; anchors.left: parent.left; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.gameId }
            Text { id: player1Name; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width*2/9; anchors.left: gameId.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.player1Name }
            Text { id: player2Name; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width*2/9; anchors.left: player1Name.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.player2Name }
            Text { id: gameState;   horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width*4/9; anchors.left: player2Name.right; anchors.right: parent.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: model.gameStatus }
        }

        header: Rectangle {
            height: listView.itemHeight
            width: listView.width
            radius: 10
            color: "forestgreen"
            z: 10

            Text { id: headerGameId;        horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width/9; anchors.left: parent.left; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("ID") }
            Text { id: headerPlayer1Name;   horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width*2/9; anchors.left: headerGameId.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("Player 1") }
            Text { id: headerPlayer2Name;   horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width*2/9; anchors.left: headerPlayer1Name.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("Player 2") }
            Text { id: headerGameState;     horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; font.pointSize: listView.itemHeight*0.4; width: parent.width*4/9; anchors.left: headerPlayer2Name.right; anchors.right: parent.right; anchors.top: parent.top; anchors.bottom: parent.bottom; text: qsTr("Game state") }
        }
        headerPositioning: ListView.OverlayHeader
    }
}
