import QtQuick 2.15

//This item creates a notification with a text.
//The interval determines how long the notification will be displayed.
Item
{
    id: genericNotification

    //dynamic properties
    property alias text: notificationText.text
    property int interval: 3000

    Rectangle
    {
        id: rectContainer
        anchors.fill: parent
        color: "#518dc2"
        border.width: 3
        border.color: "black"
        radius: 10

        onHeightChanged: calcTextPixelSize()
        onWidthChanged: calcTextPixelSize()

        MouseArea
        {
            id: mouseAreaCloseNotification
            anchors.fill: parent
            onClicked: __stackViewNotificationManager.popCurrentNotification()
        }

        Text
        {
            id: notificationText
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: TextSizer.getFontSizeBoundingRect(width, height, text)
            onTextChanged: calcTextPixelSize()
        }
    }

    function calcTextPixelSize()
    {
        notificationText.font.pixelSize = rectContainer.height*0.4
    }
}
