#include <string>
#include <tuple>
#include <vector>

class sdk
{
public:
    typedef enum
    {
        INVALID_MOVE = 0,
        NEXT_MOVE = 1,
        GAME_DRAWN = 2,
        PLAYER1_WON = 3,
        PLAYER2_WON = 4
    } GameStatus;

    struct GameBoard
    {
        GameStatus gameStatus;
        std::string board;

        GameBoard()
            : gameStatus(INVALID_MOVE)
            , board("")
        {}
    };
    

    //only one instance of sdk running in the program!
    static sdk& getInstance();

    void setIpAddr(const std::string addr = "localhost");

    //returns token from server, which should be used for sending requests
    bool login();
    void logout();

    uint16_t getToken() const;

    //send request to create a new game on server
    bool registerGame(const std::string& player1Name, const std::string& player2Name);

    //retrive list of all players with their scores
    const std::vector<std::string> getListOfAllPlayers();

    //retrieve list of all games and their results
    const std::vector<std::string> getListOfAllGames();

    //make a move
    GameBoard makeMove(uint8_t index);

private:
    sdk() {}

    long m_token = -1;
    std::string m_addr;
};
