#include "sdk.h"
#include <curl/curl.h>

#include <iostream>
#include <functional>

#include <rapidjson/document.h>

static size_t curlCallback(char* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

static std::string activateCURL(const std::string& url, const std::string& body)
{
    curl_global_init(CURL_GLOBAL_ALL);

    CURL* easyhandle = curl_easy_init();
    std::string respBuffer;

    curl_easy_setopt(easyhandle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(easyhandle, CURLOPT_POSTFIELDS, body.c_str());
    curl_easy_setopt(easyhandle, CURLOPT_WRITEFUNCTION, curlCallback);
    curl_easy_setopt(easyhandle, CURLOPT_WRITEDATA, &respBuffer);

    curl_easy_perform(easyhandle);

    return respBuffer;
}

sdk& sdk::getInstance()
{
    static sdk instance;
    return instance;
}

void sdk::setIpAddr(const std::string addr)
{
    m_addr = addr;
}

bool sdk::login()
{
    uint16_t hashedPwd = std::hash<std::string>{}("defaultPassword");
    std::cout << "Hashed: " << hashedPwd << std::endl;
    std::string body("client=ticTacToeClient&pwd=defaultPassword");// + std::to_string(hashedPwd));
    std::string url = "http://" + m_addr + ":9080/login";

    std::string respBuffer = activateCURL(url, body);

    std::cout << "*** LOGIN RESPONSE: " << respBuffer << std::endl;

    if(respBuffer.size() != 0)
    {
        rapidjson::Document d;
        d.Parse(respBuffer.c_str());
        rapidjson::Value& token = d["token"];
        if(token.GetInt() >= 0) 
        {
            m_token = token.GetInt();
            return true;
        }
    }

    return false; // login failed
}

void sdk::logout()
{
    std::string url = "http://" + m_addr + ":9080/logout";
    std::string body("token=" + std::to_string(m_token));

    std::string respBuffer = activateCURL(url, body);

    std::cout << "*** LOGOUT RESPONSE: " << respBuffer << std::endl;

    rapidjson::Document d;
    d.Parse(respBuffer.c_str());
    rapidjson::Value& id = d["id"];
    if(id.GetInt() == -99999)
    {
        m_token = -99999;
        m_token = -99999;
    }
}

uint16_t sdk::getToken() const
{
    return m_token;
}

bool sdk::registerGame(const std::string& player1Name, const std::string& player2Name)
{
    if(player1Name.size() < 3 || player2Name.size() < 3)
    {
        std::cout << "Player name must have at least 3 characters!" << std::endl;
        return false;
    }
    std::string url = "http://" + m_addr + ":9080/registerGame";
    std::string body("token=" + std::to_string(m_token) + "&player1Name=" + player1Name + "&player2Name=" + player2Name);

    std::string respBuffer = activateCURL(url, body);

    std::cout << "*** REGISTER RESPONSE: " << respBuffer << std::endl;

    rapidjson::Document d;
    d.Parse(respBuffer.c_str());
    rapidjson::Value& id = d["token"];
    int token = id.GetInt();
    if(token != -1) 
    {
        m_token = token;
        return true;
    }
    return false;
}

std::vector<std::string> sdk::getListOfAllPlayers()
{
    if(m_token == -1) return std::vector<std::string>{};

    std::string url = "http://" + m_addr + ":9080/getAllPlayers";
    std::string body("token=" + std::to_string(m_token));

    std::string respBuffer = activateCURL(url, body);

    std::cout << "*** ALL PLAYERS RESPONSE: " << respBuffer << std::endl;

    rapidjson::Document d;
    d.Parse(respBuffer.c_str());
    rapidjson::Value& id = d["id"];
    if(id.GetInt() == -1) return std::vector<std::string>{};

    rapidjson::Value& users = d["users"];

    std::vector<std::string> result;
    for(rapidjson::Value::ConstValueIterator iter = users.Begin(); iter != users.End(); ++iter)
    {
        result.emplace_back(iter->GetString());
    }
    return result;
}

std::vector<std::string> sdk::getListOfAllGames()
{
    if(m_token == -1) return std::vector<std::string>{};

    std::string url = "http://" + m_addr + ":9080/getListOfAllGames";
    std::string body("token=" + std::to_string(m_token));

    std::string respBuffer = activateCURL(url, body);

    std::cout << "*** RETRIEVE LIST OF ALL GAMES RESPONSE: " << respBuffer << std::endl;

    rapidjson::Document d;
    d.Parse(respBuffer.c_str());
    rapidjson::Value& id = d["id"];
    if(id.GetInt() == -1) return std::vector<std::string>{};

    rapidjson::Value& games = d["games"];

    std::vector<std::string> result;
    for(rapidjson::Value::ConstValueIterator iter = games.Begin(); iter != games.End(); ++iter)
    {
        result.emplace_back(iter->GetString());
    }
    return result;
}

sdk::GameBoard sdk::makeMove(uint8_t index)
{
    if(m_token == -1 || index < 1 || index > 9)
    {
        std::cout << "Invalid index" << std::endl;
        return GameBoard();
    }

    std::string url = "http://" + m_addr + ":9080/makeMove";
    std::string body("token=" + std::to_string(m_token) + "&index=" + std::to_string(index));

    std::string respBuffer = activateCURL(url, body);

    std::cout << "*** MAKE A MOVE RESPONSE: " << respBuffer << std::endl;

    rapidjson::Document d;
    GameBoard gameBoard;
    d.Parse(respBuffer.c_str());
    rapidjson::Value& token = d["token"];
    if(token.GetInt() == -1) return GameBoard();
    rapidjson::Value& gameState = d["gameState"];
    gameBoard.gameStatus = sdk::GameStatus(gameState.GetInt());
    rapidjson::Value& board = d["board"];
    gameBoard.board = board.GetString();

    return gameBoard;
}
