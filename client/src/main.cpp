#include <iostream>
#include "sdk.h"

static char gameBoard[3][3];

void initializeBoard()
{
    for (size_t i = 0; i < 3; i++)
    {
        for (size_t j = 0; j < 3; j++)
        {
            gameBoard[i][j] = *std::to_string(3*i + j + 1).c_str();
        }
    }
}

void drawBoard()
{
    for (size_t i = 0; i < 3; i++)
    {
        std::cout << gameBoard[i][0] << " | " << gameBoard[i][1] << " | " << gameBoard[i][2] << std::endl;
        if(i!=2)
            std::cout << "----------" << std::endl;
    }
}

void addSign(int sign, int index)
{
    char ticTac = 'O';
    if(sign == 0)
    {
        ticTac = 'X';
    }
    if(index > 0 && index < 10)
    {
        int row = 0, column = 0;
        if(index > 3 && index < 7)
        {
            row = 1;
        }
        else if (index > 6)
        {
            row = 2;
        }
        column = index - 3*row - 1;

        if(gameBoard[row][column] != 'X' && gameBoard[row][column] != 'O')
        {
            gameBoard[row][column] = ticTac;
        }
    }
}

int main()
{
    initializeBoard();
    auto& sdkInstance = sdk::getInstance();

    sdkInstance.setIpAddr(); // localhost as default
    
    sdkInstance.login();
    std::string playerName1, playerName2;

    std::cout << "Enter name for player1: ";
    std::cin >> playerName1;
    getchar();

    std::cout << "Enter name for player2: ";
    std::cin >> playerName2;
    getchar();

    int turn = 0, index = 0;
    sdk::GameBoard gameBoard;

    while(sdkInstance.registerGame(playerName1, playerName2))
    {
        drawBoard();
        std::cout << playerName1 << " to move, please choose a field: ";
        std::cin >> index;
        getchar();

        gameBoard = sdkInstance.makeMove(index);
        if(gameBoard.gameStatus == sdk::INVALID_MOVE)
        {
            std::cout << "Invalid move, choose again!" << std::endl;
            continue;
        }
        addSign(turn % 2, index);
        while (gameBoard.gameStatus != sdk::GAME_DRAWN && gameBoard.gameStatus != sdk::PLAYER1_WON && gameBoard.gameStatus != sdk::PLAYER2_WON)
        {
            if(gameBoard.gameStatus != sdk::INVALID_MOVE)
            {
                addSign(turn % 2, index);
                ++turn;
            }
            else
            {
                std::cout << "Invalid move, choose again!" << std::endl;
            }
            drawBoard();
            if(turn % 2 == 0)
            {
                std::cout << playerName1 << " to move, please choose a field: ";
                std::cin >> index;
                getchar();
            }
            else
            {
                std::cout << playerName2 << " to move, please choose a field: ";
                std::cin >> index;
                getchar();
            }
            gameBoard = sdkInstance.makeMove(index);
        }
        switch (gameBoard.gameStatus)
        {
        case sdk::GAME_DRAWN:
            std::cout << "Game is drawn!" << std::endl;
            break;

        case sdk::PLAYER1_WON:
            std::cout << playerName1 << " won!" << std::endl;
            break;


        case sdk::PLAYER2_WON:
            std::cout << playerName2 << " won!" << std::endl;
            break;

        default:
            std::cout << "Default!" << std::endl;
            break;
        }

        initializeBoard();
        char newGame;
        std::cout << "Do you want to start a new game! [Y/N]: ";
        std::cin >> newGame;
        getchar();

        if (newGame != 'Y' && newGame != 'y')
        {
            break;
        }

        std::cout << "Enter name for player1: ";
        std::cin >> playerName1;
        getchar();

        std::cout << "Enter name for player2: ";
        std::cin >> playerName2;
        getchar();

        turn = 0;
        index = 0;
        gameBoard.gameStatus = sdk::INVALID_MOVE;
    }

    sdkInstance.logout();

    return 0;
}
