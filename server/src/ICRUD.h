#ifndef TIC_TAC_TOE_ICRUD_H
#define TIC_TAC_TOE_ICRUD_H

#include <vector>

template<typename T>
class ICRUD
{
public:
    virtual long createRecord(const T& a_ref_entity) = 0;
    virtual void deleteRecord(const T& a_ref_entity) = 0;
    virtual void updateRecord(const T& a_ref_entity) = 0;
    virtual void getRecordById(long a_id, T& a_ref_entity) = 0;
    virtual std::vector<T> getAllRecords() = 0;
};

#endif //TIC_TAC_TOE_ICRUD_H
