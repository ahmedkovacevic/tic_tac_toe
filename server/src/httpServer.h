#ifndef TIC_TAC_TOE_HTTPSERVER_H
#define TIC_TAC_TOE_HTTPSERVER_H

#include <sqlite3wrapper/sqlite3wrapper.h>
#include <pistache/endpoint.h>

class httpServer : public Pistache::Http::Handler
{
HTTP_PROTOTYPE(httpServer)

typedef enum
{
    LOGIN = 0,
    REGISTER_GAME,
    GET_ALL_PLAYERS,
    GET_ALL_GAMES,
    MAKE_MOVE,
    LOGOUT,
    NOT_ALLOWED
} _allowedRequests;

typedef enum 
{
    INVALID = 0,
    NEXT_MOVE = 1,
    GAME_DRAWN = 2,
    PLAYER1_WON = 3,
    PLAYER2_WON = 4
} gameStatus;

struct ActiveGame
{
    std::string player1Name;
    std::string player2Name;
    uint8_t gameStatus;
    char sign;
    char gameBoard[3][3];

    ActiveGame()
        : player1Name("Player1")
        , player2Name("Player2")
        , gameStatus(0)
        , sign('X')
    {
        for (uint8_t i = 0; i < 3; ++i)     
        {
            for (uint8_t j = 0; j < 3; ++j)
            {
                gameBoard[i][j] = '-';
            }
        }
        
    }
};


void onRequest(const Pistache::Http::Request& req, Pistache::Http::ResponseWriter resp) override;
void onTimeout(const Pistache::Http::Request& /*req*/, Pistache::Http::ResponseWriter resp) override;
static _allowedRequests parseRequest(const std::string& req);

static std::string errorResponse(const std::string& error);
static void prepareDB();
std::string login(const std::string& request);
std::string getListOfPlayers(const std::string& request) const;
std::string registerGame(const std::string& request);
std::string getListOfGames(const std::string& request) const;
void addGameToDatabase(ActiveGame& activeGame);
std::string logout(const std::string& request);
std::string makeMove(const std::string& request);

gameStatus checkGameStatus(const ActiveGame &activeGame);
bool parseMove(int index, ActiveGame& activeGame);

public:
    ~httpServer() { m_dbInstance.closeConnectionToDatabase(); }
    inline void setPort(uint16_t port) { m_port = port; }
    void initDB();
    void run() const;

private:
    uint16_t m_port;
    SQLite3_n::SQLite3& m_dbInstance = SQLite3_n::SQLite3::getInstance();

    std::map<uint32_t, ActiveGame> m_activeGamesMap;

};


#endif //TIC_TAC_TOE_HTTPSERVER_H
