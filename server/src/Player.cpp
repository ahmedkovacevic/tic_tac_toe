#include "Player.h"

Player::Player(std::string& userName, int won, int drawn, int lost)
{
    m_id = -1;
    m_userName = userName;
    m_gamesWon = won;
    m_gamesDraw = drawn;
    m_gamesLost = lost;
}

void Player::setId(long id)
{
    m_id = id;
}

void Player::setUsername(const std::string& userName)
{
    m_userName = userName;
}

void Player::setGamesWon(int won)
{
    m_gamesWon = won;
}

void Player::setGamesDrawn(int drawn)
{
    m_gamesDraw = drawn;
}

void Player::setGamesLost(int lost)
{
    m_gamesLost = lost;
}
