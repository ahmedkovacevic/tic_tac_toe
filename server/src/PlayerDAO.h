#ifndef TIC_TAC_TOE_USERDAO_H
#define TIC_TAC_TOE_USERDAO_H

#include "sqlite3wrapper/sqlite3wrapper.h"
#include "Player.h"
#include "ICRUD.h"

#include <vector>

class PlayerDAO : public ICRUD<Player>
{
public:
    long createRecord(const Player& a_ref_entity);
    void deleteRecord(const Player& a_ref_entity);
    void updateRecord(const Player& a_ref_entity);
    void getRecordById(long a_id, Player& a_ref_entity);

    std::vector<Player> getAllRecords();

    void getUserByName(const std::string& userName, Player& a_ref_entity);

};


#endif //TIC_TAC_TOE_USERDAO_H
