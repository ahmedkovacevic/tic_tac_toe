#include "httpServer.h"

int main()
{
    httpServer server;
    server.initDB();
    server.setPort(9080);
    server.run();

    return 0;
}
