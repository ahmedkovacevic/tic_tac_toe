#ifndef TIC_TAC_TOE_CALLBACKS_H
#define TIC_TAC_TOE_CALLBACKS_H

class callbacks {
public:
    static int sqlite3Callback(void* data, int argc, char** argv, char** azColName);

    //Players callbacks
    static int PlayerSelectByIdCallback(void* data, int argc, char** argv, char** azColName);
    static int PlayerSelectByNameCallback(void* data, int argc, char** argv, char** azColName);
    static int PlayerSelectAllRecords(void* data, int argc, char** argv, char** azColName);

    //Games callbacks
    static int GameSelectByIdCallback(void* data, int argc, char** argv, char** azColName);
    static int GameSelectAllRecords(void* data, int argc, char** argv, char** azColName);

    static int SelectMaxIdCallback(void* data, int argc, char** argv, char** azColName);
};


#endif //TIC_TAC_TOE_CALLBACKS_H
