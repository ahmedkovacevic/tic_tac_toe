#ifndef TIC_TAC_TOE_GAMES_H
#define TIC_TAC_TOE_GAMES_H

#include <string>

typedef enum
{
    GAME_DRAWN = 0,
    GAME_PLAYER1_WON = 1,
    GAME_PLAYER2_WON = 2
} GameState;

class Games {
public:
    Games() {}
    ~Games() {}
    Games(const std::string& playerName1, const std::string& playerName2, const GameState& gameState);

    inline long getId() const { return m_id; }
    inline const std::string& getPlayerName1() const { return m_playerName1; }
    inline const std::string& getPlayerName2() const { return m_playerName2; }
    inline GameState getGameState() const { return m_gameState; }

    void setId(long id);
    void setPlayer1Name(const std::string& name);
    void setPlayer2Name(const std::string& name);
    void setGameState(uint32_t gameState);

private:
    long m_id;
    std::string m_playerName1;
    std::string m_playerName2;
    GameState m_gameState;
};


#endif //TIC_TAC_TOE_GAMES_H
