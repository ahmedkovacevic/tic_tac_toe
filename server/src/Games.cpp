#include "Games.h"

Games::Games(const std::string& playerName1, const std::string& playerName2, const GameState& gameState) 
    : m_id(-1)
    , m_playerName1(playerName1) 
    , m_playerName2(playerName2)
    , m_gameState(gameState)    
{}

void Games::setId(long id)
{
    m_id = id;
}

void Games::setPlayer1Name(const std::string& name)
{
    m_playerName1 = name;
}

void Games::setPlayer2Name(const std::string& name)
{
    m_playerName2 = name;
}

void Games::setGameState(uint32_t gameState)
{
    m_gameState = GameState(gameState);
}