#include "sqlite3wrapper.h"

namespace SQLite3_n
{
    SQLite3& SQLite3::getInstance()
    {
        static SQLite3 instance;
        return instance;
    }

    bool SQLite3::init()
    {
        try
        {
            //create tables in database
            //create only if table does not exists
            // IF NOT EXISTS and AUTOINCREMENT

            // Create table Players
            if (!getInstance().createTableInDatabase(
                    "CREATE TABLE IF NOT EXISTS PLAYERS (" \
                    "ID       INTEGER PRIMARY KEY AUTOINCREMENT," \
                    "userName    CHAR(50)," \
                    "gamesWon    INTEGER," \
                    "gamesDrawn  INTEGER," \
                    "gamesLost   INTEGER);"))
            {
                std::cout << "*** DBG OUT: Unable to create Players table in database!" << std::endl;
            }

            // Create table Games
            if (!getInstance().createTableInDatabase(
                    "CREATE TABLE IF NOT EXISTS GAMES ("      \
                "ID             INTEGER         PRIMARY KEY       AUTOINCREMENT," \
                "player1           CHAR(50),"                         \
                "player2           CHAR(50),"                         \
                "gameState INTEGER);"))
            {
                std::cout << "*** DBG OUT: Unable to create Games table in database!" << std::endl;
            }
        }
        catch (std::logic_error& err)
        {
            std::cout << err.what();
        }
        catch (std::runtime_error& err)
        {
            std::cout << err.what();
        }

        return true;
    }

    void SQLite3::deleteAllEntries()
    {
        const std::string sql_statement_base = "DELETE FROM ";

        try {
            (void)deleteD(sql_statement_base + "PLAYERS;");
            (void)deleteD(sql_statement_base + "GAMES;");
        }
        catch (std::logic_error& err)
        {
            std::cout << err.what();
        }
        catch (std::runtime_error& err)
        {
            std::cout << err.what();
        }
    }

    void SQLite3::setDatabaseFilePath(const std::string& a_ref_db_path)
    {
        m_path_to_db = a_ref_db_path;
    }

    bool SQLite3::connectToDatabase()
    {
        if (sqlite3_open(m_path_to_db.c_str(), &m_p_db))
        {
            std::cout << sqlite3_errmsg(m_p_db) << std::endl;
            return false; //can't open database
        }
        return true; //database opened successfully
    }

    void SQLite3::closeConnectionToDatabase()
    {
        sqlite3_close(m_p_db);
    }

    void SQLite3::assignDatabaseCallback(int(*callback)(void*, int, char**, char**))
    {
        m_p_callback = callback;
    }
    void SQLite3::assignDatabaseCallbackPointer(void* a_p_entity)
    {
        m_p_callback_obj = a_p_entity;
    }
    void SQLite3::unassignDatabaseCallback()
    {
        m_p_callback = NULL;
    }
    void SQLite3::unassignDatabaseCallbackPointer()
    {
        m_p_callback_obj = NULL;
    }

    bool SQLite3::createTableInDatabase(const std::string& a_ref_sql_statement)
    {
        if (!checkSqlite3CmdValidity(SQL_Commands::CREATE_TABLE, a_ref_sql_statement))
        {
            throw(std::logic_error("Invalid command for CREATE TABLE"));
        }
        return execSqlite3Cmd(a_ref_sql_statement);
    }

    void SQLite3::insert(const std::string& a_ref_sql_statement)
    {
        if (!checkSqlite3CmdValidity(SQL_Commands::INSERT, a_ref_sql_statement))
        {
            throw(std::logic_error("Invalid command for INSERT"));
        }
        if (!execSqlite3Cmd(a_ref_sql_statement))
        {
            throw(std::runtime_error("Error in insert operation!"));
        }
    }

    void SQLite3::select(const std::string& a_ref_sql_statement)
    {
        if (!checkSqlite3CmdValidity(SQL_Commands::SELECT, a_ref_sql_statement))
        {
            throw(std::logic_error("Invalid command for SELECT"));
        }
        if (!execSqlite3Cmd(a_ref_sql_statement))
        {
            throw(std::runtime_error("Error in select operation!"));
        }
    }

    void SQLite3::update(const std::string& a_ref_sql_statement)
    {
        if (!checkSqlite3CmdValidity(SQL_Commands::UPDATE, a_ref_sql_statement))
        {
            throw(std::logic_error("Invalid command for UPDATE"));
        }
        if (!execSqlite3Cmd(a_ref_sql_statement))
        {
            throw(std::runtime_error("Error in update operation!"));
        }
    }

    void SQLite3::deleteD(const std::string& a_ref_sql_statement)
    {
        if (!checkSqlite3CmdValidity(SQL_Commands::DELETE, a_ref_sql_statement))
        {
            throw(std::logic_error("Invalid command for DELETE"));
        }
        if (!execSqlite3Cmd(a_ref_sql_statement))
        {
            throw(std::runtime_error("Error in delete operation!"));
        }
    }

    void SQLite3::execSqlCmd(const std::string& a_ref_sql_statement)
    {
        if (!execSqlite3Cmd(a_ref_sql_statement))
        {
            throw(std::runtime_error("Error in Custom SQL operation!"));
        }
    }

    bool SQLite3::checkSqlite3CmdValidity(SQL_Commands a_command_type, const std::string& a_ref_sql_statement)
    {
        //add additional SQL commands as strings
        static std::string commands[] = { "CREATE", "INSERT", "SELECT", "UPDATE", "DELETE" };

        size_t found = a_ref_sql_statement.find(commands[a_command_type]);
        if (found == std::string::npos) return false;
        return true;
    }

    bool SQLite3::execSqlite3Cmd(const std::string& a_ref_sql_statement)
    {
        char* p_error_msg;
        int retVal = sqlite3_exec(m_p_db, a_ref_sql_statement.c_str(), m_p_callback, m_p_callback_obj, &p_error_msg);
        if (retVal != SQLITE_OK)
        {
            std::cout << p_error_msg << std::endl;
            sqlite3_free(p_error_msg);
            return false;
        }
        return true;
    }

}