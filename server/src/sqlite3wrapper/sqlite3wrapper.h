#ifndef SQLITE3WRAPPER_H
#define SQLITE3WRAPPER_H

#include "../../lib/sqlite3.h"

#include <iostream>
#include <string>
#include <functional>
#include <exception>

namespace SQLite3_n
{
    //sqlite3 wrapper
    class SQLite3 
    {
    private:
        typedef enum {
            CREATE_TABLE,
            INSERT,
            SELECT,
            UPDATE,
            DELETE
            // add additional SQL commands
        } SQL_Commands;
    public:
        static SQLite3& getInstance();

        /* creates all tables if they not exist*/
        bool init();

        /* delete all tables etries */
        void deleteAllEntries();

        /* connect to database methods */
        void setDatabaseFilePath(const std::string& dbPath);
        bool connectToDatabase();
        void closeConnectionToDatabase();

        /* callback related methods */
        void assignDatabaseCallback(int(*callback)(void*, int, char**, char**));
        void assignDatabaseCallbackPointer(void* a_p_entity);
        void unassignDatabaseCallback();
        void unassignDatabaseCallbackPointer();

        /* create table methods */
        bool createTableInDatabase(const std::string& a_ref_sql_statement);

        /* insert operation */
        void insert(const std::string& a_ref_sql_statement);

        /* select operation */
        void select(const std::string& a_ref_sql_statement);

        /* update operation */
        void update(const std::string& a_ref_sql_statement);

        /* delete operation */
        void deleteD(const std::string& a_ref_sql_statement);

        /* custom SQL command */
        void execSqlCmd(const std::string& a_ref_sql_statement);

    private:
        SQLite3() : m_p_db(nullptr), m_p_callback(nullptr) {};

        bool checkSqlite3CmdValidity(SQL_Commands a_command_type, const std::string& a_ref_sql_statement);
        bool execSqlite3Cmd(const std::string& a_ref_sql_statement);

    private:
        sqlite3* m_p_db;
        std::string m_path_to_db;

        int (*m_p_callback)(void*, int, char**, char**);
        void* m_p_callback_obj; // pointer for callback object

    };
}

#endif