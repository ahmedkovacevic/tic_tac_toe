#include "httpServer.h"
#include "callbacks.h"

#include "Player.h"
#include "PlayerDAO.h"

#include "Games.h"
#include "GamesDAO.h"

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>

#include <sstream>

std::string httpServer::errorResponse(const std::string& error)
{
    rapidjson::StringBuffer  s;
    rapidjson::Writer<rapidjson::StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("token");
    writer.Int(-1);
    writer.Key("error");
    writer.String(error.c_str());
    writer.EndObject();
    return s.GetString();
}

std::string httpServer::login(const std::string &request)
{
    std::cout << "LOGIN REQUEST" << std::endl;
    size_t startOfClientDescriptor = request.find("=");
    size_t stopOfEClientDescriptor = request.find("&");
    const std::string clientDescriptor = request.substr(startOfClientDescriptor + 1, stopOfEClientDescriptor - startOfClientDescriptor - 1);

    size_t startOfPwd = request.find("=", stopOfEClientDescriptor);
    size_t stopOfPwd = request.length();
    const std::string pwd = request.substr(startOfPwd + 1, stopOfPwd - startOfPwd - 1);

    size_t h_pwd;
    std::stringstream ss(pwd);
    ss >> h_pwd;
    if(pwd == "defaultPassword")
    {
        rapidjson::StringBuffer  s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        writer.StartObject();
        writer.Key("token");
        uint32_t gameToken = std::hash<size_t>{}(m_activeGamesMap.size());
        writer.Int(gameToken);
        m_activeGamesMap[gameToken] = ActiveGame();
        return s.GetString();
    }
    else
    {
        return errorResponse("Invalid client password!");
    }
}

// std::string httpServer::registerPlayer(const std::string &request)
// {
//     size_t starOfName = request.find("=");
//     size_t stopOfName = request.find("&");
//     const std::string PlayerName = request.substr(starOfName + 1, stopOfName - starOfName - 1);

//     PlayerDAO pDAO;
//     Player player;
//     player.setUsername(PlayerName);
//     player.setGamesWon(0);
//     player.setGamesDrawn(0);
//     player.setGamesLost(0);

//     Player fromDB;
//     fromDB.setId(-1);
//     pDAO.getUserByName(PlayerName, fromDB);

//     if(fromDB.getId() > -1)
//     {
//         return errorResponse("User already exits!");
//     }

//     long userID = pDAO.createRecord(player);
//     if(userID > -1)
//     {
//         rapidjson::StringBuffer  s;
//         rapidjson::Writer<rapidjson::StringBuffer> writer(s);
//         writer.StartObject();
//         writer.Key("id");
//         writer.Int(userID);
//         writer.Key("email:");
//         writer.String(PlayerName.c_str());
//         writer.Key("message");
//         writer.String("User registered succesfully!");
//         writer.EndObject();
//         return s.GetString();
//     }
//     else
//     {
//         return errorResponse("Unable to register user!");
//     }
// }

std::string httpServer::registerGame(const std::string &request)
{
    size_t startOfToken = request.find("=");
    size_t stopOfToken = request.find("&");
    const std::string token = request.substr(startOfToken + 1, stopOfToken - startOfToken - 1);

    size_t startOfPlayerName1 = request.find("=", stopOfToken);
    size_t stopOfPlayerName1 = request.find("&", startOfPlayerName1);
    const std::string name1 = request.substr(startOfPlayerName1 + 1, stopOfPlayerName1 - startOfPlayerName1 - 1);

    size_t startOfPlayerName2 = request.find("=", stopOfPlayerName1);
    size_t stopOfPlayerName2 = request.find("&", startOfPlayerName2);
    const std::string name2 = request.substr(startOfPlayerName2 + 1, stopOfPlayerName2 - startOfPlayerName2 - 1);

    std::cout << "Player name 1: " << name1 << " Player name 2: " << name2 << std::endl;
    uint32_t token_int;
    std::stringstream  ss(token);
    ss >> token_int;

    if(m_activeGamesMap.find(token_int) != m_activeGamesMap.end()) //check client token
    {
        Player player1, player2;
        player1.setId(-1);
        player2.setId(-1);
        PlayerDAO pDAO;
        pDAO.getUserByName(name1, player1);
        if(player1.getId() < 0) //check if player already exits
        {
            player1.setUsername(name1);
            player1.setGamesWon(0);
            player1.setGamesLost(0);
            player1.setGamesDrawn(0);
            pDAO.createRecord(player1);
        }

        pDAO.getUserByName(name2, player2);
        if(player2.getId() < 0) //check if player already exits
        {
            player2.setUsername(name2);
            player2.setGamesWon(0);
            player2.setGamesLost(0);
            player2.setGamesDrawn(0);
            pDAO.createRecord(player2);
        }

        m_activeGamesMap[token_int].player1Name = name1;
        m_activeGamesMap[token_int].player2Name = name2;
        rapidjson::StringBuffer  s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        writer.StartObject();
        writer.Key("gameId");
        writer.Int(player2.getId());
        writer.Key("token");
        writer.Int(token_int);
        writer.Key("message");
        writer.String("Player added succesfully!");
        writer.EndObject();
        return s.GetString();
    }
    else //wrong token
    {
        return errorResponse("Wrong user token!");
    }
}

bool httpServer::parseMove(int index, ActiveGame& activeGame)
{
    if(index > 0 && index < 10)
    {
        int row = 0, column = 0;
        if(index > 3 && index < 7)
        {
            row = 1;
        }
        else if (index > 6)
        {
            row = 2;
        }
        column = index - 3*row - 1;

        if(activeGame.gameBoard[row][column] == '-')
        {
            activeGame.gameBoard[row][column] = activeGame.sign;
            return true; // valid move
        }
    }
    return false; // invalid move
}

httpServer::gameStatus httpServer::checkGameStatus(const ActiveGame &activeGame)
{
    bool win = false;
    for (size_t i = 0; i < 3; ++i)
    {
        // check rows
        if(activeGame.gameBoard[i][0] == activeGame.gameBoard[i][1]
        && activeGame.gameBoard[i][0] == activeGame.gameBoard[i][2]
        && activeGame.gameBoard[i][0] != '-')
        {
            win = true;
            break;
        }

        //check columns
        if(activeGame.gameBoard[0][i] == activeGame.gameBoard[1][i]
        && activeGame.gameBoard[0][i] == activeGame.gameBoard[2][i]
        && activeGame.gameBoard[0][i] != '-')
        {
            win = true;
            break;
        }
    }

    // check diagonals
    if(activeGame.gameBoard[0][0] == activeGame.gameBoard[1][1]
    && activeGame.gameBoard[0][0] == activeGame.gameBoard[2][2]
    && activeGame.gameBoard[0][0] != '-')
    {
        win = true;
    }

    if(activeGame.gameBoard[0][2] == activeGame.gameBoard[1][1]
    && activeGame.gameBoard[0][2] == activeGame.gameBoard[2][0]
    && activeGame.gameBoard[0][2] != '-')
    {
        win = true;
    }

    if(win == true && activeGame.sign == 'X')
    {
        std::cout << "Player1 won " << activeGame.sign << std::endl;
        return PLAYER1_WON;
    }
    else if(win == true && activeGame.sign == 'O')
    {
        std::cout << "Player2 won" << activeGame.sign << std::endl;
        return PLAYER2_WON;
    }

    // check if game is drawn

    for (size_t i = 0; i < 3; i++)
    {
        for (size_t j = 0; j < 3; j++)
        {
            if(activeGame.gameBoard[i][j] == '-')
            {
                return NEXT_MOVE;
            }
        }
    }
    return GAME_DRAWN;
}

std::string httpServer::makeMove(const std::string& request)
{
    std::cout << "MAKE MOVE " << request << std::endl;
    size_t startOfToken = request.find("=");
    size_t stopOfToken = request.find("&");
    const std::string token = request.substr(startOfToken + 1, stopOfToken - startOfToken - 1);

    size_t startOfIndex = request.find("=", stopOfToken);
    size_t stopOfIndex = request.find("&");
    const std::string index = request.substr(startOfIndex + 1, stopOfIndex - startOfIndex - 1);

    int index_int;
    std::stringstream ss1(index);
    ss1 >> index_int;

    uint32_t token_int;
    std::stringstream  ss(token);
    ss >> token_int;

    if(m_activeGamesMap.find(token_int) != m_activeGamesMap.end())
    {
        if(m_activeGamesMap[token_int].gameStatus != GAME_DRAWN
        && m_activeGamesMap[token_int].gameStatus != PLAYER1_WON
        && m_activeGamesMap[token_int].gameStatus != PLAYER2_WON)
        {
            if(parseMove(index_int, m_activeGamesMap[token_int]))
            {
                gameStatus status = checkGameStatus(m_activeGamesMap[token_int]);
                m_activeGamesMap[token_int].sign = m_activeGamesMap[token_int].sign == 'X' ? 'O' : 'X';
                m_activeGamesMap[token_int].gameStatus = status;
                std::string board="";
                for (size_t i = 0; i < 3; ++i)
                {
                    for (size_t j = 0; j < 3; ++j)
                    {
                        board += std::to_string(3*i+j+1) + "=" + m_activeGamesMap[token_int].gameBoard[i][j] + "&";
                    }
                    
                }
                
                if(m_activeGamesMap[token_int].gameStatus == GAME_DRAWN
                || m_activeGamesMap[token_int].gameStatus == PLAYER1_WON
                || m_activeGamesMap[token_int].gameStatus == PLAYER2_WON)
                {
                    addGameToDatabase(m_activeGamesMap[token_int]);
                    m_activeGamesMap[token_int] = ActiveGame();
                }
                rapidjson::StringBuffer  s;
                rapidjson::Writer<rapidjson::StringBuffer> writer(s);
                writer.StartObject();
                writer.Key("token");
                writer.Int(token_int);
                writer.Key("gameState");
                writer.Int(status);
                writer.Key("board");
                writer.String(board.c_str());
                writer.EndObject();
                return s.GetString();
            }
            else
            {
                return errorResponse("Invalid move!");
            }
        }
        else
        {
            return errorResponse("Game finished!");
        }
    }
    else
    {
        return errorResponse("Invalid client token!");
    }
}

void httpServer::addGameToDatabase(ActiveGame& activeGame)
{
    Games game;
    GamesDAO gDAO;

    game.setId(-1);
    game.setPlayer1Name(activeGame.player1Name);
    game.setPlayer2Name(activeGame.player2Name);
    game.setGameState(activeGame.gameStatus - 2);

    gDAO.createRecord(game);

    Player player1, player2;
    PlayerDAO pDAO;

    pDAO.getUserByName(activeGame.player1Name, player1);
    pDAO.getUserByName(activeGame.player2Name, player2);

    if(activeGame.gameStatus == PLAYER1_WON)
    {
        player1.setGamesWon(player1.getGamesWon()+1);
        player2.setGamesLost(player2.getGamesLost()+1);
    }
    else if(activeGame.gameStatus == PLAYER2_WON)
    {
        player2.setGamesWon(player2.getGamesWon()+1);
        player1.setGamesLost(player1.getGamesLost()+1);
    }
    else
    {
        player1.setGamesDrawn(player1.getGamesDrawn()+1);
        player2.setGamesDrawn(player2.getGamesDrawn()+1);
    }

    std::cout << "Debug 1: " << player1.getId() << " " << player2.getId() << std::endl;
    pDAO.updateRecord(player1);
    pDAO.updateRecord(player2);
}

std::string httpServer::getListOfGames(const std::string& request) const
{
    size_t startOfToken = request.find("=");
    size_t stopOfToken = request.find("&");
    const std::string token = request.substr(startOfToken + 1, stopOfToken - startOfToken - 1);

    uint32_t token_int;
    std::stringstream  ss(token);
    ss >> token_int;

    if(m_activeGamesMap.find(token_int) != m_activeGamesMap.end())
    {
        GamesDAO gDAO;
        std::vector<Games> games = gDAO.getAllRecords();

        rapidjson::StringBuffer  s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        writer.StartObject();
        writer.Key("id");
        writer.Int(1);
        writer.Key("games");
        writer.StartArray();
        for(auto& game : games)
        {
            std::string gameInfo{"id=" + std::to_string(game.getId()) + "&player1Name=" + game.getPlayerName1() + "&player2Name=" + game.getPlayerName2() + "&gameState=" + std::to_string(game.getGameState())};
            writer.String(gameInfo.c_str());
        }
        writer.EndArray();
        writer.EndObject();
        return s.GetString();
    }
    else
    {
        return errorResponse("Wrong client token!");
    }
}

std::string httpServer::getListOfPlayers(const std::string& request) const
{
    size_t startOfToken = request.find("=");
    size_t stopOfToken = request.find("&");
    const std::string token = request.substr(startOfToken + 1, stopOfToken - startOfToken - 1);

    uint32_t token_int;
    std::stringstream  ss(token);
    ss >> token_int;

    if(m_activeGamesMap.find(token_int) != m_activeGamesMap.end())
    {
        PlayerDAO pDAO;
        std::vector<Player> players = pDAO.getAllRecords();

        rapidjson::StringBuffer  s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        writer.StartObject();
        writer.Key("id");
        writer.Int(1);
        writer.Key("users");
        writer.StartArray();
        for(auto& player : players)
        {
            std::string playerInfo = "userName=" + player.getUserName() 
                                   + "&gamesWon=" + std::to_string(player.getGamesWon()) 
                                   + "&gamesDrawn=" + std::to_string(player.getGamesDrawn())
                                   + "&gamesLost=" + std::to_string(player.getGamesLost());
            
            writer.String(playerInfo.c_str());
        }
        writer.EndArray();
        writer.EndObject();
        return s.GetString();
    }
    else
    {
        return errorResponse("Wrong client token!");
    }
}

std::string httpServer::logout(const std::string &request)
{
    size_t startOfToken = request.find("=");
    size_t stopOfToken = request.find("&");
    const std::string token = request.substr(startOfToken + 1, stopOfToken - startOfToken - 1);

    uint32_t token_int;
    std::stringstream  ss(token);
    ss >> token_int;

    if(m_activeGamesMap.find(token_int) != m_activeGamesMap.end()) //check user token
    {
        rapidjson::StringBuffer  s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        writer.StartObject();
        writer.Key("id");
        writer.Int(-99999);
        writer.Key("token");
        writer.Int(-99999);
        writer.Key("message");
        writer.String("User logged out!");
        writer.EndObject();
        return s.GetString();
    }
    else //wrong token
    {
        return errorResponse("Wrong user token!");
    }
}

void httpServer::onRequest(const Pistache::Http::Request &req, Pistache::Http::ResponseWriter resp)
{
    std::cout << "*** NEW REQUEST: " << req.resource() + req.body() << std::endl;
    const std::string request(req.resource());
    switch(parseRequest(request))
    {
        case LOGIN:
            resp.send(Pistache::Http::Code::Ok, login(req.body()));
            break;
        case REGISTER_GAME:
            resp.send(Pistache::Http::Code::Ok, registerGame(req.body()));
            break;
        case GET_ALL_PLAYERS:
            resp.send(Pistache::Http::Code::Ok, getListOfPlayers(req.body()));
            break;
        case GET_ALL_GAMES:
            resp.send(Pistache::Http::Code::Ok, getListOfGames(req.body()));
            break;
        case MAKE_MOVE:
            resp.send(Pistache::Http::Code::Ok, makeMove(req.body()));
            break;
        case LOGOUT:
            resp.send(Pistache::Http::Code::Ok, logout(req.body()));
            break;
        default:
            break;
    }
}

void httpServer::onTimeout(const Pistache::Http::Request &, Pistache::Http::ResponseWriter resp)
{
    resp.send(Pistache::Http::Code::Request_Timeout, "Timeout").then([=](ssize_t) {}, PrintException());
}

httpServer::_allowedRequests httpServer::parseRequest(const std::string &req)
{
    //login client
    if(req == "/login") return _allowedRequests::LOGIN;

    //register game
    if(req == "/registerGame") return _allowedRequests::REGISTER_GAME;

    //get all players
    if(req == "/getAllPlayers") return _allowedRequests::GET_ALL_PLAYERS;

    //get all games
    if(req == "/getListOfAllGames") return _allowedRequests::GET_ALL_GAMES;

    //make move
    if(req == "/makeMove") return _allowedRequests::MAKE_MOVE;

    //logout
    if(req == "/logout") return _allowedRequests ::LOGOUT;

    //not allowed
    return _allowedRequests::NOT_ALLOWED;
}

void httpServer::prepareDB()
{
    std::cout << "*** DATABASE PREPARATION STARTED PLEASE WAIT!" << std::endl;
    // add users
    // Player premium1("first@premium.com", "first", "premium"); // no playlists
    // Player premium2("second@premium.com", "second", "premium"); // 2x playlists, first < 20 songs, second > 800
    // Player free("free@free.com", "free", "free"); // playlist with one song
    // Player admin("admin@admin.com", "admin", "admin"); // playlist with > 2k3 songs
    // PlayerDAO uDAO;
    // premium1.setId(uDAO.createRecord(premium1));
    // premium2.setId(uDAO.createRecord(premium2));
    // free.setId(uDAO.createRecord(free));
    // admin.setId(uDAO.createRecord(admin));

    // // add songs
    // Games song;
    // GamesDAO sDAO;
    // for(int i = 0; i < 3000; i++)
    // {
    //     std::string title{"title" + std::to_string(i)};
    //     song.setTitle(title);
    //     sDAO.createRecord(song);
    // }

    // PlaylistSong pls;
    // PlaylistSongsDAO plsDAO;

    // // add playlists
    // Playlists playlist;
    // PlaylistsDAO plDAO;

    // // second@premium.com
    // playlist.setName("firstForSecondUser");
    // playlist.setUser(premium2);
    // playlist.setId(plDAO.createRecord(playlist));
    // pls.setPlaylist(playlist);
    // for(int i = 1; i < 18; i++)
    // {
    //     sDAO.getRecordById(i, song);
    //     pls.setSong(song);
    //     plsDAO.createRecord(pls);
    // }

    // playlist.setName("secondForSecondUser");
    // playlist.setUser(premium2);
    // playlist.setId(plDAO.createRecord(playlist));
    // pls.setPlaylist(playlist);
    // for(int i = 1; i < 850; i++)
    // {
    //     sDAO.getRecordById(i, song);
    //     pls.setSong(song);
    //     plsDAO.createRecord(pls);
    // }

    // // free@free.com
    // playlist.setName("firstForFreeUser");
    // playlist.setUser(free);
    // playlist.setId(plDAO.createRecord(playlist));
    // pls.setPlaylist(playlist);
    // sDAO.getRecordById(1000, song);
    // pls.setSong(song);
    // plsDAO.createRecord(pls);

    // // admin@admin.com
    // playlist.setName("firstForAdminUser");
    // playlist.setUser(admin);
    // playlist.setId(plDAO.createRecord(playlist));
    // pls.setPlaylist(playlist);
    // for(int i = 1; i < 2800; i++)
    // {
    //     sDAO.getRecordById(i, song);
    //     pls.setSong(song);
    //     plsDAO.createRecord(pls);
    // }

    std::cout << "*** DATABASE IS PREPARED, HTTP SERVER IS RUNNING!" << std::endl;
}

void httpServer::initDB()
{
    m_dbInstance.setDatabaseFilePath("../db/test.db");
    if(!m_dbInstance.connectToDatabase())
    {
        std::cout << "*** DBG OUT: Unable to connect to database!" << std::endl;
        exit(0);
    }

    m_dbInstance.assignDatabaseCallback(callbacks::sqlite3Callback);
    m_dbInstance.init();
}

void httpServer::run() const
{
    Pistache::Port port(m_port);
    Pistache::Address addr(Pistache::Ipv4::any(), port);
    auto server = std::make_shared<Pistache::Http::Endpoint>(addr);
    auto opt = Pistache::Http::Endpoint::options().threads(1);
    server->init(opt);
    server->setHandler(Pistache::Http::make_handler<httpServer>());
    server->serve();
}
