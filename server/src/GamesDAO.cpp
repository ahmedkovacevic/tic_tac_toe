#include "GamesDAO.h"
#include "callbacks.h"

long GamesDAO::createRecord(const Games &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();
    long max_id = -1;
    dbInstance.assignDatabaseCallback(callbacks::SelectMaxIdCallback);
    dbInstance.assignDatabaseCallbackPointer(&max_id);

    std::string sql_statement = "SELECT IFNULL(max(ID), 0) FROM GAMES;";
    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();

    sql_statement =
            "INSERT INTO GAMES (ID, player1, player2, gameState) " \
            "VALUES (" + std::to_string(max_id + 1) + ", '" + \
            a_ref_entity.getPlayerName1() + "', '" + \
            a_ref_entity.getPlayerName2() + "', '" + \
            std::to_string(a_ref_entity.getGameState()) + "');";

    dbInstance.insert(sql_statement);
    return (max_id + 1);
}

void GamesDAO::deleteRecord(const Games &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    std::string sql_statement =
            "DELETE FROM FAMES " \
            "WHERE ID = " + std::to_string(a_ref_entity.getId()) + ";";

    dbInstance.deleteD(sql_statement);
}

void GamesDAO::updateRecord(const Games &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    std::string sql_statement =
            "UPDATE GAMES SET " \
            "player1 = '" + a_ref_entity.getPlayerName1() + "', " + \
            "player2 = '" + a_ref_entity.getPlayerName2() + "', " + \
            "gameState = '" + std::to_string(a_ref_entity.getGameState()) + "' " + \
            "WHERE ID = " + std::to_string(a_ref_entity.getId()) + ";";

    dbInstance.update(sql_statement);
}

void GamesDAO::getRecordById(long a_id, Games &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    dbInstance.assignDatabaseCallback(callbacks::GameSelectByIdCallback);
    dbInstance.assignDatabaseCallbackPointer(&a_ref_entity);

    std::string sql_statement = "SELECT ID, player1, player2, gameState FROM GAMES WHERE ID = " + std::to_string(a_id) + ";";

    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();
}

std::vector<Games> GamesDAO::getAllRecords()
{
    std::vector<Games> a_vec_records;
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();
    std::string sql_statement = "SELECT ID, player1, player2, gameState from GAMES;";

    dbInstance.assignDatabaseCallback(callbacks::GameSelectAllRecords);
    dbInstance.assignDatabaseCallbackPointer(&a_vec_records);
    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();

    return a_vec_records;
}