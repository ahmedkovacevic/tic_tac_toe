#include "callbacks.h"
#include "Player.h"
#include "Games.h"
#include "PlayerDAO.h"
#include "GamesDAO.h"

#include <iostream>
#include <vector>
#include <string>

int callbacks::sqlite3Callback(void *data, int argc, char **argv, char **azColName)
{
    int i;
    std::cout << data << std::endl;

    for (i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }

    printf("\n");
    return 0;
}

int callbacks::PlayerSelectByIdCallback(void *data, int argc, char **argv, char **azColName)
{
    (void)argc; (void)azColName;
    Player* p_player = static_cast<Player*>(data);

    p_player->setId(std::atoi(argv[0]));
    p_player->setUsername(std::string(argv[1]));
    p_player->setGamesWon(std::atoi(argv[2]));
    p_player->setGamesDrawn(std::atoi(argv[3]));
    p_player->setGamesLost(std::atoi(argv[4]));

    return 0;
}

int callbacks::PlayerSelectByNameCallback(void* data, int argc, char** argv, char** azColName)
{
    (void)argc; (void)azColName;
    Player* p_player = static_cast<Player*>(data);

    p_player->setId(std::atoi(argv[0]));
    p_player->setUsername(std::string(argv[1]));
    p_player->setGamesWon(std::atoi(argv[2]));
    p_player->setGamesDrawn(std::atoi(argv[3]));
    p_player->setGamesLost(std::atoi(argv[4]));

    return 0;
}

int callbacks::PlayerSelectAllRecords(void* data, int argc, char** argv, char** azColName)
{
    (void)argc; (void)azColName;
    std::vector<Player>* p_vec_players = static_cast<std::vector<Player>*>(data);

    std::string name = std::string(argv[1]);
    Player player(name, std::atoi(argv[2]), std::atoi(argv[3]), std::atoi(argv[4]));
    player.setId(std::atoi(argv[0]));

    p_vec_players->push_back(player);
    return 0;
}

int callbacks::GameSelectByIdCallback(void *data, int argc, char **argv, char **azColName)
{
    (void)argc; (void)azColName;
    Games* p_song = static_cast<Games*>(data);

    p_song->setId(std::atoi(argv[0]));
    p_song->setPlayer1Name(std::string(argv[1]));
    p_song->setPlayer2Name(std::string(argv[2]));
    p_song->setGameState(std::atoi(argv[3]));

    return 0;
}

int callbacks::GameSelectAllRecords(void *data, int argc, char **argv, char **azColName)
{
    (void)argc; (void)azColName;
    std::vector<Games>* p_vec_games = static_cast<std::vector<Games>*>(data);
    std::string playerName1 = std::string(argv[1]), playerName2 = std::string(argv[2]);
    GameState gameState = GameState(std::atoi(argv[3]));
    Games game(playerName1, playerName2, gameState);
    game.setId(std::atoi(argv[0]));

    p_vec_games->push_back(game);
    return 0;
}

int callbacks::SelectMaxIdCallback(void* data, int argc, char** argv, char** azColName)
{
    (void)argc; (void)azColName;
    // if callback is called, that means we have something selected
    long* max_id = static_cast<long*>(data);
    *max_id = std::atol(argv[0]);

    return 0;
}
