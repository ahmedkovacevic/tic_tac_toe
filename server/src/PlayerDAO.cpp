#include "PlayerDAO.h"
#include "callbacks.h"

long PlayerDAO::createRecord(const Player &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();
    long max_id = -1;
    dbInstance.assignDatabaseCallback(callbacks::SelectMaxIdCallback);
    dbInstance.assignDatabaseCallbackPointer(&max_id);

    std::string sql_statement = "SELECT IFNULL(max(ID), 0) FROM PLAYERS;";
    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();

    sql_statement =
            "INSERT INTO PLAYERS (ID, userName, gamesWon, gamesDrawn, gamesLost) " \
            "VALUES (" + std::to_string(max_id + 1) + ", '" + \
            a_ref_entity.getUserName() + "', '" + \
            std::to_string(a_ref_entity.getGamesWon()) + "', '" + \
            std::to_string(a_ref_entity.getGamesDrawn()) + "', '" + \
            std::to_string(a_ref_entity.getGamesLost()) + "'); ";

    dbInstance.insert(sql_statement);
    return (max_id + 1);
}

void PlayerDAO::deleteRecord(const Player &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    std::string sql_statement =
            "DELETE FROM PLAYERS "  \
            "WHERE ID = " + std::to_string(a_ref_entity.getId()) + ";";

    dbInstance.deleteD(sql_statement);
}

void PlayerDAO::updateRecord(const Player &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    std::string sql_statement =
            "UPDATE PLAYERS SET " \
            "userName = '" + a_ref_entity.getUserName() + "', " + \
            "gamesWon = " + std::to_string(a_ref_entity.getGamesWon()) + ", " + \
            "gamesDrawn = " + std::to_string(a_ref_entity.getGamesDrawn()) + ", " + \
            "gamesLost = " + std::to_string(a_ref_entity.getGamesLost()) + " " + \
            "WHERE ID = " + std::to_string(a_ref_entity.getId()) + ";";

    dbInstance.update(sql_statement);
}

void PlayerDAO::getRecordById(long a_id, Player &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    dbInstance.assignDatabaseCallback(callbacks::PlayerSelectByIdCallback);
    dbInstance.assignDatabaseCallbackPointer(&a_ref_entity);

    std::string sql_statement = "SELECT ID, userName, gamesWon, gamesDrawn, gamesLost FROM PLAYERS WHERE ID = " + std::to_string(a_id) + ";";

    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();
}

std::vector<Player> PlayerDAO::getAllRecords()
{
    std::vector<Player> a_vec_records;
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();
    std::string sql_statement = "SELECT ID, userName, gamesWon, gamesDrawn, gamesLost from PLAYERS;";

    dbInstance.assignDatabaseCallback(callbacks::PlayerSelectAllRecords);
    dbInstance.assignDatabaseCallbackPointer(&a_vec_records);
    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();

    return a_vec_records;
}

void PlayerDAO::getUserByName(const std::string &userName, Player &a_ref_entity)
{
    auto& dbInstance = SQLite3_n::SQLite3::getInstance();

    dbInstance.assignDatabaseCallback(callbacks::PlayerSelectByNameCallback);
    dbInstance.assignDatabaseCallbackPointer(&a_ref_entity);

    std::string sql_statement = "SELECT ID, userName, gamesWon, gamesDrawn, gamesLost FROM PLAYERS WHERE userName = '" + userName + "';";

    dbInstance.select(sql_statement);
    dbInstance.unassignDatabaseCallback();
    dbInstance.unassignDatabaseCallbackPointer();
}