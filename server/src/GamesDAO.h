#ifndef TIC_TAC_TOE_SONGSDAO_H
#define TIC_TAC_TOE_SONGSDAO_H

#include "sqlite3wrapper/sqlite3wrapper.h"
#include "Games.h"
#include "ICRUD.h"

#include <vector>

class GamesDAO : public ICRUD<Games>
{
public:
    long createRecord(const Games& a_ref_entity);
    void deleteRecord(const Games& a_ref_entity);
    void updateRecord(const Games& a_ref_entity);
    void getRecordById(long a_id, Games& a_ref_entity);

    std::vector<Games> getAllRecords();
};


#endif //TIC_TAC_TOE_SONGSDAO_H
