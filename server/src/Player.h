#ifndef TIC_TAC_TOE_PLAYER_H
#define TIC_TAC_TOE_PLAYER_H

#include <string>
#include <functional>

class Player {
public:
    Player() {}
    ~Player() {}
    Player(std::string& userName, int won, int drawn, int lost);

    inline long getId() const { return m_id; }
    inline const std::string& getUserName() const { return m_userName; }
    inline int getGamesWon() const { return m_gamesWon; }
    inline int getGamesDrawn() const { return m_gamesDraw; }
    inline int getGamesLost() const { return m_gamesLost; }

    void setId(long id);
    void setUsername(const std::string& userName);
    void setGamesWon(int won);
    void setGamesDrawn(int drawn);
    void setGamesLost(int lost);

private:
    long m_id;
    std::string m_userName;
    int m_gamesWon;
    int m_gamesDraw;
    int m_gamesLost;
};


#endif //TIC_TAC_TOE_PLAYER_H
