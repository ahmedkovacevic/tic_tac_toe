Built and tested only on Ubuntu 20.04 LTS
dependencies:
- cmake (for ubuntu:$ sudo snap install cmake --classic )
- Pistache (for ubuntu):
	$ sudo add-apt-repository ppa:pistache+team/unstable
	$ sudo apt update
	$ sudo apt install libpistache-dev
- rapidjson (for ubuntu):
	$ sudo apt-get update -y
	$ sudo apt-get install -y rapidjson-dev

- curl dev library:
	$sudo apt-get install libcurl4-gnutls-dev -y
	
Build SDK (SDK source is in sdk_src):
- Navigate to client/sdk_build folder in terminal and execute commands:
	$ cmake .
	$ make

There will be a libsdk.a file (which is a library for sdk), copy libsdk.a into client/lib/bin folder.

Build client:
- Navigate to client/build folder in terminal and execute commands:
	$ cmake .
	$ make
	
Build server:
- Navigate to server/build folder in terminal and execute commands:
	$ cmake .
	$ make

Test CI/CD runner.
